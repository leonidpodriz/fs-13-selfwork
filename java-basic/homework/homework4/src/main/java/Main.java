package main.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Pet p1 = new Pet();
        Pet p2 = new Pet(PetSpecies.cat, "murzik");
        Pet p3 = new Pet(PetSpecies.cat, "matroskin", 2, 90, new String[]{});

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);

        Human h1 = new Human();
        Human h2 = new Human("N", "S", 1980);
        Human h3 = new Human("N", "S", 1980);
        Human h4 = new Human("N", "S", 1980, 90, null, new String[][]{{"Monday", "do work"}});

        System.out.println(h1);
        System.out.println(h2);
        System.out.println(h3);
        System.out.println(h4);

        Family f1 = new Family(h1, h2);
        Family f2 = new Family(h2, h4);

        System.out.println(f1);
        System.out.println(f2);
        System.out.printf("Family #1 count: %d\n", f1.countFamily());
        System.out.printf("Family #2 count: %d\n", f2.countFamily());

        f1.addChild(h3);
        System.out.println(f1);
        System.out.printf("Family #1 count: %d\n", f1.countFamily());

        boolean b = f1.deleteChild(0);
        System.out.println(f1);
        System.out.printf("[%s] Family #1 count: %d\n", b, f1.countFamily());
    }
}
