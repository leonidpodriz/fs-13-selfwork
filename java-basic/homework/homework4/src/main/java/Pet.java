package main.java;

import java.util.Arrays;
import java.util.Objects;

enum PetSpecies {
    dog, cat, fish, undefined
}

public class Pet {
    PetSpecies species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits;

    public Pet(PetSpecies species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(PetSpecies species, String nickname) {
        this(species, nickname, 0, 0, new String[]{});
    }

    public Pet() {
        this(PetSpecies.undefined, "no-name");
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", nickname);
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    public String getNickname() {
        return nickname;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public PetSpecies getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return String.format(
                "%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
                species,
                nickname,
                age,
                trickLevel,
                Arrays.toString(habits)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet pet)) return false;
        return getAge() == pet.getAge() && getTrickLevel() == pet.getTrickLevel() && getSpecies() == pet.getSpecies() && getNickname().equals(pet.getNickname()) && Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getSpecies(), getNickname(), getAge(), getTrickLevel());
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
}
