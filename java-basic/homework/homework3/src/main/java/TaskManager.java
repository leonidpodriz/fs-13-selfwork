package main.java;

import java.util.Scanner;

public class TaskManager {
    static Scanner scanner = new Scanner(System.in);
    static String[][] schedule = new String[7][2];
    static boolean isExited = false;

    static String INPUT_DAY = "Please, input the day of the week:";
    static String INVALID_COMMAND = "Sorry, I don't understand you, please try again.";

    static final String SUNDAY = "Sunday";
    static final String MONDAY = "Monday";
    static final String TUESDAY = "Tuesday";
    static final String WEDNESDAY = "Wednesday";
    static final String THURSDAY = "Thursday";
    static final String FRIDAY = "Friday";
    static final String SATURDAY = "Saturday";
    static final String EXIT = "Exit";

    static void fillScheduleWithDays() {
        schedule[0][0] = SUNDAY;
        schedule[1][0] = MONDAY;
        schedule[2][0] = TUESDAY;
        schedule[3][0] = WEDNESDAY;
        schedule[4][0] = TUESDAY;
        schedule[5][0] = FRIDAY;
        schedule[6][0] = SATURDAY;
    }

    static void fillScheduleWithTasks() {
        schedule[0][1] = "do home work";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][1] = "do home work";
        schedule[3][1] = "do home work";
        schedule[4][1] = "do home work";
        schedule[5][1] = "do home work";
        schedule[6][1] = "do home work";
    }

    static void onStart() {
        fillScheduleWithDays();
        fillScheduleWithTasks();
    }


    static void print(String text) {
        System.out.println(text);
    }

    static void printTaskByDay(int day) {
        print(schedule[day][1]);
    }

    static String getUserInput(String message) {
        print(message);
        return scanner.nextLine();
    }

    static String getProcessedString(String string) {
        String trimmed_string = string.trim();
        String first_upper_case_letter = trimmed_string.substring(0, 1).toUpperCase();
        String string_rest =trimmed_string.substring(1).toLowerCase();

        return String.format("%s%s", first_upper_case_letter, string_rest);
    }

    static String getUserCommand() {
        String command = getUserInput(INPUT_DAY);
        return getProcessedString(command);
    }

    static void runMainLoop() {
        while (!isExited) {
            String command = getUserCommand();

            switch (command) {
                case SUNDAY -> printTaskByDay(0);
                case MONDAY -> printTaskByDay(1);
                case TUESDAY -> printTaskByDay(2);
                case WEDNESDAY -> printTaskByDay(3);
                case THURSDAY -> printTaskByDay(4);
                case FRIDAY -> printTaskByDay(5);
                case SATURDAY -> printTaskByDay(6);
                case EXIT -> isExited = true;
                default -> print(INVALID_COMMAND);
            }
        }
    }


    public static void main(String[] args) {
        onStart();
        runMainLoop();
    }
}
