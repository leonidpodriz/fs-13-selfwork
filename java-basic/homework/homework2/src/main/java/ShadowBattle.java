package main.java;

import java.util.Random;
import java.util.Scanner;

public class ShadowBattle {
    static int x;
    static int y;

    static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();

    static boolean NEEDS_HINT = true;

    static String WELCOME = "All set. Get ready to rumble!";
    static String BYE = "You have won!";
    static String HINT_FORMAT = "HINT: (%d, %d)%n";
    static String X_INPUT_TEXT = "Enter x: ";
    static String Y_INPUT_TEXT = "Enter y: ";
    static String INVALID_VALUE = "Value is invalid. Try again: ";

    static short AREA_WIGHT = 5;
    static short AREA_HEIGHT = 5;

    static String MISSED_SYMBOL = "*";
    static String BLANK_SYMBOL = "-";
    static String HIT_SYMBOL = "X";
    static String TABLE_SEPARATOR = " | ";

    static short TARGET = -1;
    static short BLANK = 0;
    static short MISSED = 1;
    static short HIT = 2;

    static short[][] battleArea = new short[AREA_HEIGHT][AREA_WIGHT];

    static void printPoint(short point) {
        if (point == HIT) {
            print(HIT_SYMBOL);
        } else if (point == MISSED) {
            print(MISSED_SYMBOL);
        } else {
            print(BLANK_SYMBOL);
        }
    }

    static void print(String text) {
        System.out.print(text);
    }

    static void println(String text) {
        print(text + "\n");
    }

    static void printWelcome() {
        println(WELCOME);
    }

    static void printGoodBye() {
        println(BYE);
    }

    static void printBreakLine() {
        print("\n");
    }

    static void printHintIfNeeds(int x, int y) {
        if (NEEDS_HINT) {
            String hint = String.format(HINT_FORMAT, x, y);
            println(hint);
        }
    }

    static void printBattleAreaHeader() {
        for (int col = 0; col <= battleArea[0].length; col++) {
            print(String.format("%d", col));
            printTableSeparator();
        }

        printBreakLine();
    }

    static void printTableSeparator() {
        print(TABLE_SEPARATOR);
    }

    static void printTableRowNumber(int number) {
        print(String.format("%d", number));
        printTableSeparator();
    }

    static void printGameSettings() {
        String settings = String.format(
                "\n=== GAME SETTINGS ===\nColumns: %d\nRows: %d\n",
                AREA_WIGHT,
                AREA_HEIGHT
        );
        println(settings);
    }

    static void printBattleTable() {
        printBattleAreaHeader();

        for (int idx = 0; idx < battleArea.length; idx++) {
            short[] line = battleArea[idx];

            printTableRowNumber(idx + 1);

            for (short point : line) {
                printPoint(point);
                printTableSeparator();
            }

            printBreakLine();
        }

        printBreakLine();
    }

    static int getRandomCoordinateWithLimit(short limit) {
        return random.nextInt(limit) + 1;
    }

    static int getRandomX() {
        return getRandomCoordinateWithLimit(AREA_WIGHT);
    }

    static int getRandomY() {
        return getRandomCoordinateWithLimit(AREA_HEIGHT);
    }

    static void setTarget(int x, int y) {
        setPoint(x, y, TARGET);
    }

    static void setRandomTargets() {
        int x = getRandomX();
        int y = getRandomY();

        printHintIfNeeds(x, y);
        setTarget(x, y);
    }

    static void setUserChoice(int x, int y) {
        short point = getPoint(x, y);

        if (point == TARGET) {
            setPoint(x, y, HIT);
        } else if (point == BLANK) {
            setPoint(x, y, MISSED);
        }
    }

    static void setPoint(int x, int y, short value) {
        battleArea[y - 1][x - 1] = value;
    }

    static short getPoint(int x, int y) {
        return battleArea[y - 1][x - 1];
    }

    static boolean isHit(int x, int y) {
        return getPoint(x, y) == HIT;
    }

    static boolean shouldGameContinue() {
        return !isHit(x, y);
    }

    static int getUserCoordinateWithLimits(int min, int max) {
        int coordinate = scanner.nextInt();

        if (coordinate > max || coordinate <= min) {
            print(INVALID_VALUE);
            coordinate = getUserCoordinateWithLimits(min, max);
        }

        return coordinate;
    }

    static int getUserX() {
        print(X_INPUT_TEXT);
        return getUserCoordinateWithLimits(0, AREA_WIGHT);
    }

    static int getUserY() {
        print(Y_INPUT_TEXT);
        return getUserCoordinateWithLimits(0, AREA_HEIGHT);
    }

    static void requestUserChoice() {
        x = getUserX();
        y = getUserY();

        setUserChoice(x, y);
    }

    static void runGameIteration() {
        requestUserChoice();
        printBattleTable();
    }

    static void onGameStart() {
        printWelcome();
        printGameSettings();
    }

    static void onGameEnd() {
        printGoodBye();
    }

    public static void main(String[] args) {
        onGameStart();
        setRandomTargets();

        do {
            runGameIteration();
        } while (shouldGameContinue());

        onGameEnd();
    }
}
