package lesson03;

import java.util.Random;

public class WorkWithArrays_1 {
    static int ARRAY_LENGTH = 10;
    static int MAX_ITEM_VALUE = 10;
    static Random random = new Random();
    static int[] ints = new int[ARRAY_LENGTH];

    public static void main(String[] args) {

        for (int idx = 0; idx < ints.length; idx++) {
            ints[idx] = random.nextInt(MAX_ITEM_VALUE);
        }

        for (int number : ints) {
            if (number % 2 == 0) {
                System.out.println(number);
            }
        }
    }
}
